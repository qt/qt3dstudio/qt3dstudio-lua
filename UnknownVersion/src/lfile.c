/*
** $Id: lfile.c,v 1 
** lua file system wrapper allowing users to override file access functions
** added by chrisn@nvidia.com
*/


#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#define lapi_c
#define LUA_CORE
#include "lua.h"

lua_user_file_io_ptr g_lua_user_file_io = NULL;

LUA_API void lua_set_file_io( lua_user_file_io_ptr file_io )
{
	g_lua_user_file_io = file_io;
}

LUA_API void*	lua_fopen( const char* filename, const char* mode, char  *absfilename )
{
	if( absfilename ) strcpy( absfilename, filename );
	if ( g_lua_user_file_io )
		return g_lua_user_file_io->user_fopen(filename, mode, absfilename );	
	return fopen( filename, mode );
}

LUA_API int	lua_fclose( void* file )
{
	if ( g_lua_user_file_io )
		return g_lua_user_file_io->user_fclose( file );
	else
		return fclose( (FILE*)file );
}

LUA_API int		lua_getc( void* file )
{
	if ( g_lua_user_file_io )
		return g_lua_user_file_io->user_getc( file );
	else
		return getc( (FILE*) file );
}

LUA_API int	lua_ungetc( int c, void* file )
{
	if ( g_lua_user_file_io )
		return g_lua_user_file_io->user_ungetc( c, file );
	else
		return ungetc( c, (FILE*) file );
}

LUA_API char* lua_fgets( char* ptr, int count, void* file )
{
	if ( g_lua_user_file_io )
		return g_lua_user_file_io->user_fgets( ptr, count, file );
	else
		return fgets(ptr, count, (FILE*) file );
}

LUA_API size_t	lua_fread( void* ptr, size_t size, size_t count, void* file )
{
	if ( g_lua_user_file_io )
		return g_lua_user_file_io->user_fread( ptr, size, count, file );
	else
		return fread( ptr, size, count, (FILE*) file );
}

LUA_API int		lua_ferror( void* file )
{
	if ( g_lua_user_file_io )
		return g_lua_user_file_io->user_ferror( file );
	else
		return ferror( (FILE*) file );
}

LUA_API int		lua_feof( void* file )
{
	if ( g_lua_user_file_io )
		return g_lua_user_file_io->user_feof( file );
	else
		return feof( (FILE*) file );
}

LUA_API int		lua_fread_number( void* file, lua_Number* d )
{
	if ( g_lua_user_file_io )
		return g_lua_user_file_io->user_fread_double( file, d );
	else
        {
            // temp variable hack needed to compile on Linux and macOS
            float temp = (float)(*d);
            return fscanf( (FILE*)file, LUA_NUMBER_SCAN, &temp );
        }
}

LUA_API void lua_clearerr( void* file )
{
	if( g_lua_user_file_io )
		 g_lua_user_file_io->user_clearerr( file );
	else
		clearerr( (FILE*)file );
}
