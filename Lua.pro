TARGET = Lua
include($$PWD/../../commonplatform.pri)
CONFIG += console warn_off installed
win32 {
DEFINES += LUA_ANSI LUA_BUILD_AS_DLL LUA_DL_DLL
}
linux | qnx {
CONFIG += staticlib
DEFINES += LUA_ANSI LUA_USE_DLOPEN
}
mac {
CONFIG += staticlib
DEFINES += LUA_ANSI LUA_USE_DLOPEN
}
android {
DEFINES += LUA_NO_LOCALECONV
}
integrity {
CONFIG += staticlib
DEFINES += LUA_ANSI
}

include(Lua.pri)

load(qt_helper_lib)
